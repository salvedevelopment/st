# Basic Technical Assessment

* Time - around 1 hour

### Please build a small API that a product could use to satisfy the following:

* Get a list of clinics
* Get a list of patients at a clinic
* Update a patient's date of birth
* Get a list of appointments at a patient/clinic

### Notes

* This is to assess how you approach and structure your code
* Does not need to be a fully functioning API
* Does not need to integrate with a backend data store (mock any data you need)
* Data model is at your discretion
* Pick a language that you deem most appropriate
* Pick a software development process that you deem most appropriate